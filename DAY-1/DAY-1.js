//test
console.log("Day-1 of 30 days of Javascript challenge !! ")


// 1. Write a single line comment which says, comments can make code readable
    //comments can make code readable

// 2. Write another single comment which says, Welcome to 30DaysOfJavaScript
    //Welcome to 30DaysOfJavaScript

// 3. Write a multiline comment which says, comments can make code readable, easy to reuse and informative
    /* comments can make code readable, 
    easy to reuse and informative */

//4. Create a variable.js file and declare variables and assign string, boolean, undefined and null data types

    let day1_String = "firstday";
    let isMarried = false;
    let future;
    let ego = null;

//5. Create datatypes.js file and use the JavaScript typeof operator to check different data types. Check the data type of each variable

    console.log(typeof day1_String);
    console.log(typeof isMarried);
    console.log(typeof future);
    console.log(typeof ego);

//6. Declare four variables without assigning values

    let udVariable_1,udVariable_2,udVariable_3,udVariable_4;

//7. Declare four variables with assigned values

    let asVariable_1 = 1;
    let asVariable_2 = "two";
    let asVariable_3 = true;
    let asVariable_4 = null;

//8. Declare variables to store your first name, last name, marital status, country and age in multiple lines

    let my_firstName = "Shreyas";
    let my_lastName = "Gangadhar";
    let my_marritalStatus = "Single";
    let my_country ="India";
    let my_age=26;

//9. Declare variables to store your first name, last name, marital status, country and age in a single line

    let my_firstName_1 = "shreyas", my_lastName_1="Gangadhar", my_marritalStatus_1="Single",my_country_1="India",my_age_1=26;

//10. Declare two variables myAge and yourAge and assign them initial values and log to the browser console.

    let myAge=26;
    let yourAge=22;
    console.log("I'm "+ myAge + " years old !!");
    console.log("You are "+ yourAge + " years old !!");